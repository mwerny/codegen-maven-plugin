package net.iceflow.oss.maven.codegen.mojo.exception;

public class SourceGenerationException extends RuntimeException {
	
	public SourceGenerationException() {
		super();
	}
	
	public SourceGenerationException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
	public SourceGenerationException(final String message) {
		super(message);
	}
	
	public SourceGenerationException(final Throwable cause) {
		super(cause);
	}
}
