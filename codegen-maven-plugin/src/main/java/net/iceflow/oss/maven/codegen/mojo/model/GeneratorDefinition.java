package net.iceflow.oss.maven.codegen.mojo.model;

public class GeneratorDefinition {
	
	private String generatorClass;
	
	public final String getGeneratorClass() {
		return generatorClass;
	}
	
	public final void setGeneratorClass(final String generatorClass) {
		this.generatorClass = generatorClass;
	}
}
