package net.iceflow.oss.maven.codegen.mojo;

import net.iceflow.oss.maven.codegen.api.GenerationContext;
import net.iceflow.oss.maven.codegen.api.GeneratorHandler;
import net.iceflow.oss.maven.codegen.api.PackageBuilder;
import net.iceflow.oss.maven.codegen.mojo.exception.SourceGenerationException;

import java.io.*;

/**
 * 
 */
public class PackageBuilderImpl implements PackageBuilder {
	
	private final File file;
	
	private final String parentPackageName;
	
	private final String packageName;
	
	private final String commonHeaderClassPathResource;
	
	private final GenerationContext context;
	
	/**
	 * @param file
	 * @param parentPackageName
	 * @param packageName
	 * @param commonHeaderClassPathResource
	 * @param context
	 */
	public PackageBuilderImpl(final File file, final String parentPackageName, final String packageName, final String commonHeaderClassPathResource, final GenerationContext context) {
		this.file = file;
		this.parentPackageName = parentPackageName;
		this.packageName = packageName;
		this.commonHeaderClassPathResource = commonHeaderClassPathResource;
		this.context = context;
		file.mkdirs();
	}
	
	@Override
	public PackageBuilder withPackage(final String name) {
		final StringBuilder sb = new StringBuilder(parentPackageName);
		sb.append(".");
		sb.append(name);
		
		return new PackageBuilderImpl(new File(file, name), sb.toString(), name, commonHeaderClassPathResource, context);
	}
	
	@Override
	public PackageBuilder withPackageInfo(final String classPathResource) {
		final File packageInfoFile = new File(file, "package-info.java");
		
		try (
			final InputStream commonInputStream = this.getClass().getResourceAsStream(commonHeaderClassPathResource);
			final InputStreamReader commonInputStreamReader = new InputStreamReader(commonInputStream);
			final BufferedReader commonReader = new BufferedReader(commonInputStreamReader);
			final InputStream inputStream = this.getClass().getResourceAsStream(classPathResource);
			final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			final BufferedReader reader = new BufferedReader(inputStreamReader);
			final FileOutputStream outputStream = new FileOutputStream(packageInfoFile);
			final PrintWriter writer = new PrintWriter(outputStream, true);
		) {
			writer.println("/**");
			
			String line = commonReader.readLine();
			
			while (line != null) {
				writer.print(" * ");
				writer.println(line);
				
				line = commonReader.readLine();
			}
			
			writer.println(" */");
			writer.println();
			
			writer.println("/**");
			
			line = reader.readLine();
			
			while (line != null) {
				writer.print(" * ");
				writer.println(line);
				
				line = reader.readLine();
			}
			
			writer.println(" */");
			
			final String packageStatement = String.format("package %s;", parentPackageName);
			writer.println(packageStatement);
		} catch (final Exception e) {
			throw new SourceGenerationException(e);
		}
		
		return this;
	}
	
	@Override
	public PackageBuilder withJavaSourceFile(final String className, final GeneratorHandler handler) {
		final File javaSourceFile = new File(file, className + ".java");
		
		try (
			final InputStream commonInputStream = this.getClass().getResourceAsStream(commonHeaderClassPathResource);
			final InputStreamReader commonInputStreamReader = new InputStreamReader(commonInputStream);
			final BufferedReader commonReader = new BufferedReader(commonInputStreamReader);
			final FileOutputStream outputStream = new FileOutputStream(javaSourceFile);
			final PrintWriter writer = new PrintWriter(outputStream, true);
		) {
			writer.println("/**");
			
			String line = commonReader.readLine();
			
			while (line != null) {
				writer.print(" * ");
				writer.println(line);
				
				line = commonReader.readLine();
			}
			
			writer.println(" */");


			final String packageStatement = String.format("package %s;", parentPackageName);
			writer.println(packageStatement);
			writer.println();

			handler.generate(className, writer, context);
		} catch (final Exception e) {
			throw new SourceGenerationException(e);
		}
		
		return this;
	}
}
