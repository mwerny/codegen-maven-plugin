package net.iceflow.oss.maven.codegen.mojo;

import java.io.File;
import java.util.List;

import net.iceflow.oss.maven.codegen.api.GenerationContext;
import net.iceflow.oss.maven.codegen.api.Generator;
import net.iceflow.oss.maven.codegen.api.PackageBuilder;
import net.iceflow.oss.maven.codegen.mojo.model.GeneratorDefinition;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "generate")
public class CodegenMojo extends AbstractMojo {
	
	@Parameter(defaultValue="${project.build.directory}/codegen-generated-sources", property="codegen.generatedSources", required=true)
	private File generatedSourcesDirectory;
	
	@Parameter(defaultValue="${project.build.directory}/codegen-generated-test-sources", property="codegen.generatedTestSources", required=true)
	private File generatedTestSourcesDirectory;
	
	@Parameter(defaultValue="${project}", required=true, readonly=true)
	private MavenProject project;
	
	@Parameter
	private List<GeneratorDefinition> generators;
	
	@Parameter
	private List<GeneratorDefinition> testGenerators;
	
	@Parameter(defaultValue="${codegen.commonHeaderClassPathResource}")
	private String commonHeaderClassPathResource;
	
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		init();
		generateCode();
		generateTestCode();
		attachSources();
		attachTestSources();
	}
	
	private void init() throws MojoFailureException {
		if (commonHeaderClassPathResource == null) {
			commonHeaderClassPathResource = "/net/iceflow/oss/maven/codegen/mojo/defaultCommonHeader.txt";
		}
		
		if (!generatedSourcesDirectory.exists()) {
			generatedSourcesDirectory.mkdirs();
		} else if (!generatedSourcesDirectory.isDirectory()) {
			final String message = String.format("Path %s already exists and is not a directory.", generatedSourcesDirectory.getAbsolutePath());
			throw new MojoFailureException(message);
		}
	}
	
	private void generateCode() throws MojoFailureException {
		if (generators == null || generators.isEmpty()) {
			return;
		}
		
		final GenerationAdapter adapter = new GenerationAdapter(getLog(), getGeneratedSourcesDirectory());
		
		for (final GeneratorDefinition definition : generators) {
			executeDefinition(definition, adapter);
		}
	}
	
	private void generateTestCode() throws MojoFailureException {
		if (testGenerators == null || testGenerators.isEmpty()) {
			return;
		}
		
		final GenerationAdapter adapter = new GenerationAdapter(getLog(), getGeneratedTestSourcesDirectory());
		
		for (final GeneratorDefinition definition : testGenerators) {
			getLog().info("Processing class: " + definition.getGeneratorClass());
			executeDefinition(definition, adapter);
		}
	}
	
	private void executeDefinition(final GeneratorDefinition definition, final GenerationAdapter adapter) throws MojoFailureException {
		final String generatorClassName = definition.getGeneratorClass();
		
		try {
			final Class<?> generatorClass = Class.forName(generatorClassName);
			final Generator generator = (Generator) generatorClass.newInstance();
			generator.execute(adapter);
		} catch (final ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			final String message = String.format("Error instantiating generator class %s.", generatorClassName);
			throw new MojoFailureException(message, e);
		}
	}
	
	private void attachSources() {
		project.addCompileSourceRoot(generatedSourcesDirectory.getAbsolutePath());
	}
	
	private void attachTestSources() {
		project.addTestCompileSourceRoot(generatedTestSourcesDirectory.getAbsolutePath());
	}
	
	public final File getGeneratedSourcesDirectory() {
		return generatedSourcesDirectory;
	}
	
	public final void setGeneratedSourcesDirectory(final File generatedSourcesDirectory) {
		this.generatedSourcesDirectory = generatedSourcesDirectory;
	}
	
	public final File getGeneratedTestSourcesDirectory() {
		return generatedTestSourcesDirectory;
	}
	
	public final void setGeneratedTestSourcesDirectory(final File generatedTestSourcesDirectory) {
		this.generatedTestSourcesDirectory = generatedTestSourcesDirectory;
	}
	
	final class GenerationAdapter implements GenerationContext {
		
		private final Log log;
		
		private final File generatedSourcesDirectory;
		
		GenerationAdapter(final Log log, final File generatedSourcesDirectory) {
			this.log = log;
			this.generatedSourcesDirectory = generatedSourcesDirectory;
		}
		
		@Override
		public File getGeneratedSourcesDirectory() {
			return generatedSourcesDirectory;
		}
		
		@Override
		public PackageBuilder withPackage(final String name) {
			return new PackageBuilderImpl(new File(getGeneratedSourcesDirectory(), name), name, name, commonHeaderClassPathResource, this);
		}
		
		@Override
		public boolean isDebugEnabled() {
			return log.isDebugEnabled();
		}
		
		@Override
		public void debug(final CharSequence content) {
			log.debug(content);
		}
		
		@Override
		public void debug(final CharSequence content, final Throwable error) {
			log.debug(content, error);
		}
		
		@Override
		public void debug(final Throwable error) {
			log.debug(error);
		}
		
		@Override
		public boolean isInfoEnabled() {
			return log.isInfoEnabled();
		}
		
		@Override
		public void info(final CharSequence content) {
			log.info(content);
		}
		
		@Override
		public void info(final CharSequence content, final Throwable error) {
			log.info(content, error);
		}
		
		@Override
		public void info(final Throwable error) {
			log.info(error);
		}
		
		@Override
		public boolean isWarnEnabled() {
			return log.isWarnEnabled();
		}
		
		@Override
		public void warn(final CharSequence content) {
			log.warn(content);
		}
		
		@Override
		public void warn(final CharSequence content, final Throwable error) {
			log.warn(content, error);
		}
		
		@Override
		public void warn(final Throwable error) {
			log.warn(error);
		}
		
		@Override
		public boolean isErrorEnabled() {
			return log.isErrorEnabled();
		}
		
		@Override
		public void error(final CharSequence content) {
			log.error(content);
		}
		
		@Override
		public void error(final CharSequence content, final Throwable error) {
			log.error(content, error);
		}
		
		@Override
		public void error(final Throwable error) {
			log.error(error);
		}
	}
}
