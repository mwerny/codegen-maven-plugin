package net.iceflow.oss.maven.codegen.api;


/**
 * 
 */
public interface PackageBuilder {
	
	/**
	 * @param name
	 * @return
	 */
	PackageBuilder withPackage(String name);
	
	/**
	 * @param classPathResource
	 * @return
	 */
	PackageBuilder withPackageInfo(String classPathResource);

	/**
	 * @param className
	 * @param handler
	 * @return
	 */
	PackageBuilder withJavaSourceFile(String className, GeneratorHandler handler);
	
}
