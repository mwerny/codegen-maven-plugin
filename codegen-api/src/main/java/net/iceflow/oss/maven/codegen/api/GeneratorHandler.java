package net.iceflow.oss.maven.codegen.api;

import java.io.PrintWriter;

public interface GeneratorHandler {
	
	void generate(String className, PrintWriter writer, GenerationContext context);
	
}
