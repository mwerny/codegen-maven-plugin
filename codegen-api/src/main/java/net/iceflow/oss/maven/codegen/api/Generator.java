package net.iceflow.oss.maven.codegen.api;

public interface Generator {
	
	/**
	 * 
	 * @param context
	 */
	void execute(GenerationContext context);
	
}
