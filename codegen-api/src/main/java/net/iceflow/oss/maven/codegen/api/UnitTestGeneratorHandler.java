package net.iceflow.oss.maven.codegen.api;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UnitTestGeneratorHandler implements GeneratorHandler {
	
	private final List<GeneratorHandler> handlers = new ArrayList<>();
	
	@Override
	public void generate(final String className, final PrintWriter writer, final GenerationContext context) {
		
		writer.println("import org.junit.Test;");
		writer.println("import static junit.framework.Assert.*;");
		writer.println();
		writer.println("public class " + className + " {");
		
		for (final GeneratorHandler handler : handlers) {
			handler.generate(className, writer, context);
		}
		
		writer.println("\t");
		writer.println("}");
	}
	
	public UnitTestGeneratorHandler withTest(final String testName, final GeneratorHandler handler) {
		final TestGeneratorHandler testHandler = new TestGeneratorHandler(testName, handler);
		handlers.add(testHandler);
		
		return this;
	}
	
	final static class TestGeneratorHandler implements GeneratorHandler {
		
		private final String testName;
		
		private final GeneratorHandler childHandler;
		
		TestGeneratorHandler(final String testName, final GeneratorHandler childHandler) {
			this.testName = testName;
			this.childHandler = childHandler;
		}
		
		@Override
		public void generate(final String className, final PrintWriter writer, final GenerationContext context) {
			writer.println("\t");
			writer.println("\t@Test");
			writer.println("\tpublic void " + testName + "() {");
			
			childHandler.generate(className, writer, context);
			
			writer.println("\t}");
		}
	}
}
